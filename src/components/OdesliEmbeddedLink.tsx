import React from 'react'

interface IOdesliEmbeddedLinkProps
{
  href: string
}

export default function OdesliEmbeddedLink( { href }: IOdesliEmbeddedLinkProps)
{
  return (
    <iframe
      title="embedded player"
      src={ `https://embed.song.link/?url=${encodeURIComponent( href )}&theme=dark` }

      width="100%"
      height="52"
      frameBorder="0"

      allowFullScreen
      sandbox="allow-same-origin allow-scripts allow-presentation allow-popups allow-popups-to-escape-sandbox">
      </iframe>
  )
}
