import React, { useState } from 'react'

interface IFlipOnClickProps {
  faces: JSX.Element[],
}

/**
 * TODO: idea, have top layer slide left, introduce buttons on the right to mess w embed
 * @param param0 
 */
export default function FlipOnClick( { faces }: IFlipOnClickProps ) 
{
  const [ currentSideIndex, setCurrentSide ] = useState(0)

  const flipSides = () =>
  {
    setCurrentSide( ( currentSideIndex + 1 ) % 2 ) 
  }

  return (
    <div className='flipboard-face' onClick={ flipSides }>
      { faces[ currentSideIndex ] }
    </div>
  )
}
