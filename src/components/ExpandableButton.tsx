import React, { useState } from 'react';

interface IExpandableButtonProps {
  text: string,
  short_title: string,
  children: JSX.Element[],
  className?: string,
}
interface IExpandableButtonState
{
  expanded: boolean,
}

export default function ExpandableButton( props: IExpandableButtonProps ) 
{
  const title: string = props.text
  const short_title: string = props.short_title
  const children: JSX.Element[] = props.children
  const [ expanded, setExpanded ] = useState(false)
    
  const toggleExpand = () => 
  {
    setExpanded(!expanded)
  }

  return (
    <div className={`expandable-container ${props.className}`} onClick={toggleExpand}>
      <div className={`border button ${expanded ? 'slid-left' : ''}`}>
        <p className="title">{ expanded ? short_title : title }</p>
      </div>
      <div className={ `expandable-children ${expanded ? '' : 'hidden'}` }>
        { children }
      </div>
    </div>
  )
}
