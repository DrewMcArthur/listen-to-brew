import React from 'react'

interface ILinkTreeButtonProps
{
  href: string,
  text: string,
  className?: string,
}
export default function LinkTreeButton(props: ILinkTreeButtonProps) 
{
  const { href, text, className } = props

  return (
    <a
      href={ href } target="_blank" rel="noopener noreferrer"
      className="link-tree-button-link"
    >
      <div className={`link-tree-button-container border ${className ? className : ""}`}>
        <p className="link-tree-button-text">{ text }</p>
      </div>
    </a>
  )
}
