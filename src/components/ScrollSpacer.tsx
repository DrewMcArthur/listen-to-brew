import React from 'react'
export default function ScrollSpacer(props: { height?: string, width?: string })
{
  const { height, width } = props
  const styleObj = height ? {
    "height": height
  } : {
    "width": width
  }
  return (
    <div style={ styleObj }></div>
  )
}
